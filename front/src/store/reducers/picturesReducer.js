import {
    CREATE_PICTURE_FAILURE, CREATE_PICTURE_REQUEST,
    CREATE_PICTURE_SUCCESS,
    DELETE_PICTURE_FAILURE,
    DELETE_PICTURE_REQUEST, DELETE_PICTURE_SUCCESS,
    FETCH_PICTURES_FAILURE,
    FETCH_PICTURES_REQUEST,
    FETCH_PICTURES_SUCCESS, FETCH_USER_PICTURES_FAILURE,
    FETCH_USER_PICTURES_REQUEST,
    FETCH_USER_PICTURES_SUCCESS,
} from "../actions/picturesActions";

const initialState = {
    pictures: [],
    fetchLoading: false,
    usersPictures: [],
    usersPictureLoading: false,
    deleteLoading: false,
    createLoading: false,
    createError: null,

};

const picturesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PICTURES_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_PICTURES_SUCCESS:
            return {...state,  fetchLoading: false, pictures: action.payload};
        case FETCH_PICTURES_FAILURE:
            return {...state, fetchLoading: false};
        case FETCH_USER_PICTURES_REQUEST:
            return {...state, usersPictureLoading: true};
        case FETCH_USER_PICTURES_SUCCESS:
            return {...state,  usersPictureLoading: false, usersPictures: action.payload};
        case FETCH_USER_PICTURES_FAILURE:
            return {...state, usersPictureLoading: false};
        case DELETE_PICTURE_REQUEST:
            return {...state, deleteLoading: true};
        case DELETE_PICTURE_SUCCESS:
            return {
                ...state,
                deleteLoading: false,
                pictures: state.pictures.filter(p=>p._id!==action.payload),
            }
        case DELETE_PICTURE_FAILURE:
            return {...state, singleLoading: false};
        case CREATE_PICTURE_REQUEST:
            return {...state, createLoading: true};
        case CREATE_PICTURE_SUCCESS:
            return {...state, createLoading: false, createError: null};
        case CREATE_PICTURE_FAILURE:
            return {...state, createLoading: false, createError: action.payload};
        default:
            return state;
    }
};

export default picturesReducer;