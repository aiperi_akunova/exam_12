import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import usersReducer, {initialState} from "./reducers/usersReducer";
import thunk from "redux-thunk";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import axiosApi from "../axiosApi";
import picturesReducer from "./reducers/picturesReducer";

const rootReducer = combineReducers({
    'users': usersReducer,
    "pictures": picturesReducer,

});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();


const store = createStore(
    rootReducer,
    persistedState,
    composeEnhancers(
        applyMiddleware(thunk)
    ));


store.subscribe(()=>{
    saveToLocalStorage({
        users:{
            ...initialState,
            user: store.getState().users.user
        },
    });
});

axiosApi.interceptors.request.use(config=>{
    try {
        config.headers['Authorization']= store.getState().users.user.token;
    }catch (e) {}
    return config;
});

axiosApi.interceptors.response.use(res=> res, e =>{
  if(!e.response){
      e.response = {data:{global: 'No internet'}};
  }
  throw e;
});

export default store;