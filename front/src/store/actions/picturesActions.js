import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";

export const FETCH_PICTURES_REQUEST = 'FETCH_PICTURES_REQUEST';
export const FETCH_PICTURES_SUCCESS = 'FETCH_PICTURES_SUCCESS';
export const FETCH_PICTURES_FAILURE = 'FETCH_PICTURES_FAILURE';

export const FETCH_USER_PICTURES_REQUEST = 'FETCH_USER_PICTURES_REQUEST';
export const FETCH_USER_PICTURES_SUCCESS = 'FETCH_USER_PICTURES_SUCCESS';
export const FETCH_USER_PICTURES_FAILURE = 'FETCH_USER_PICTURES_FAILURE';

export const DELETE_PICTURE_REQUEST = 'DELETE_PICTURE_REQUEST';
export const DELETE_PICTURE_SUCCESS = 'DELETE_PICTURE_SUCCESS';
export const DELETE_PICTURE_FAILURE = 'DELETE_PICTURE_FAILURE';

export const CREATE_PICTURE_REQUEST = 'CREATE_PICTURE_REQUEST';
export const CREATE_PICTURE_SUCCESS = 'CREATE_PICTURE_SUCCESS';
export const CREATE_PICTURE_FAILURE = 'CREATE_PICTURE_FAILURE';

export const deletePictureRequest = () => ({type: DELETE_PICTURE_REQUEST});
export const deletePictureSuccess = id => ({type: DELETE_PICTURE_SUCCESS, payload: id});
export const deletePictureFailure = () => ({type: DELETE_PICTURE_FAILURE});

export const fetchPicturesRequest = () => ({type: FETCH_PICTURES_REQUEST});
export const fetchPicturesSuccess = data => ({type: FETCH_PICTURES_SUCCESS, payload: data});
export const fetchPicturesFailure = () => ({type: FETCH_PICTURES_FAILURE});

export const fetchUsersPicturesRequest = () => ({type: FETCH_USER_PICTURES_REQUEST});
export const fetchUsersPicturesSuccess = data => ({type: FETCH_USER_PICTURES_SUCCESS, payload: data});
export const fetchUsersPicturesFailure = () => ({type: FETCH_USER_PICTURES_FAILURE});

export const createPictureRequest = () => ({type: CREATE_PICTURE_REQUEST});
export const createPictureSuccess = () => ({type: CREATE_PICTURE_SUCCESS});
export const createPictureFailure = error => ({type: CREATE_PICTURE_FAILURE, payload: error});


export const fetchPictures = () => {
    return async (dispatch) => {
        try {
            dispatch(fetchPicturesRequest());
            const response = await axiosApi.get('/pictures');
            dispatch(fetchPicturesSuccess(response.data));
        } catch (error) {
            toast.error('Can not fetch pictures!');
            dispatch(fetchPicturesFailure());
        }
    };
};

export const fetchUsersPictures = (id) => {
    return async (dispatch) => {
        try {
            dispatch(fetchUsersPicturesRequest());
            const response = await axiosApi.get('/pictures/'+id);
            dispatch(fetchUsersPicturesSuccess(response.data));
        } catch (error) {
            toast.error('Can not fetch pictures!');
            dispatch(fetchUsersPicturesFailure());
        }
    };
};

export const deletePicture = id => {
    return async (dispatch) => {
        try {
            dispatch(deletePictureRequest());
            await axiosApi.delete('/pictures/'+id);
            dispatch(deletePictureSuccess(id));
            toast.success('Successfully deleted');
            dispatch(historyPush('/'));
        } catch (error) {
            if (error.response.status === 403) {
                toast.error('Permission denied');
            }
            toast.error('Could not delete');
            dispatch(deletePictureFailure(error));
        }
    };
};

export const createPicture = pictureData => {
    return async dispatch => {
        try {
            dispatch(createPictureRequest());
            await axiosApi.post('/pictures', pictureData);
            dispatch(createPictureSuccess());
            dispatch(historyPush('/'));
            toast.success('Successfully was created!');
        } catch (error) {
            dispatch(createPictureFailure(error.response.data));
            toast.error('Could not create picture');
        }
    };
};
