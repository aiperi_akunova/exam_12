import React, {useEffect} from 'react';
import {CircularProgress, Grid, makeStyles} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchUsersPictures} from "../../store/actions/picturesActions";
import PictureItem from "../../components/PictureItem/PictureItem";
import {Link} from "react-router-dom";

const useStyles = makeStyles(theme => ({
    btn: {
        border: "1px solid black",
        margin: "0 0 0 20px",
        backgroundColor: "darkblue",
        color: "white",
        textDecoration: "none",
        display: "block",
        padding:"10px 25px",
        textAlign: "center"
    },
}));


const UsersPictures =({match})=>{
    const classes=useStyles();
    const dispatch = useDispatch();
    const userPictures = useSelector(state => state.pictures.usersPictures);
    const fetchLoading = useSelector(state => state.pictures.usersPictureLoading);
    const user = useSelector(state => state.users.user);
    const id = match.params.id;

    let showBtn=false;
    if(user && user._id === id){
        showBtn = true;
    }

    useEffect(() => {
        dispatch(fetchUsersPictures(id));
    }, [dispatch, id]);

    return (
        <Grid container direction="column" spacing={2}>
            {showBtn && (
                <Link to='/add' className={classes.btn} >Add new picture</Link>
            )}
            <Grid item container justifyContent='center' direction="row" spacing={1}>
                {fetchLoading ? (
                    <Grid container justifyContent="center" alignItems="center">
                        <Grid item>
                            <CircularProgress/>
                        </Grid>
                    </Grid>
                ) : userPictures.map(picture => (
                    <PictureItem
                        key={picture._id}
                        id={picture._id}
                        userId={picture.user._id}
                        picture={picture.image}
                        title={picture.title}
                        user={picture.user.display_name}
                        showBtn={showBtn}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default UsersPictures;