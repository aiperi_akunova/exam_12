import React, {useEffect} from 'react';
import {CircularProgress, Grid} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchPictures} from "../../store/actions/picturesActions";
import PictureItem from "../../components/PictureItem/PictureItem";


const Main = () => {
    const dispatch = useDispatch();
    const pictures = useSelector(state => state.pictures.pictures);
    const fetchLoading = useSelector(state => state.pictures.fetchLoading);

    useEffect(() => {
        dispatch(fetchPictures());
    }, [dispatch]);

    return (
            <Grid container direction="column" spacing={2}>
                <Grid item container justifyContent='center' direction="row" spacing={1}>
                    {fetchLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : pictures.map(picture => (
                        <PictureItem
                            key={picture._id}
                            userId={picture.user._id}
                            picture={picture.image}
                            title={picture.title}
                            user={picture.user.display_name}
                        />
                    ))}
                </Grid>
            </Grid>
    );
};

export default Main;