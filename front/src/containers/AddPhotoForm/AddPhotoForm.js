import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import {Grid} from "@material-ui/core";
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import FileInput from "../../components/UI/FileInput/FileInput";
import {createPicture} from "../../store/actions/picturesActions";
import {useDispatch, useSelector} from "react-redux";

const useStyles = makeStyles(theme => ({
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    container:{
        maxWidth: "70%",
        margin: "0 auto",
        marginTop: theme.spacing(2)
    },
    title: {
        textAlign: "center",
        color: "darkblue"
    }
}));

const AddPhotoForm = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector(state => state.pictures.createError);
    console.log(error)
    const loading = useSelector(state => state.pictures.createLoading);

    const [picture, setPicture] = useState({
        title: "",
        image: null,

    });

    const submitFormHandler = e => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(picture).forEach(key => {
            formData.append(key, picture[key]);
        });

        dispatch(createPicture(formData));
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value=e.target.value;
        setPicture(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setPicture(prevState => {
            return {...prevState, [name]: file};
        });
    };

    const getFieldError = fieldName => {
        try {
            return error.error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <Grid
            container
            direction="column"
            spacing={2}
            component="form"
            autoComplete="off"
            onSubmit={submitFormHandler}
            className={classes.container}
            noValidate
        >
                <h1 className={classes.title}>Add new picture!</h1>
            <FormElement
                required
                label="Title"
                name="title"
                value={picture.title}
                onChange={inputChangeHandler}
                error={getFieldError('title')}

            />

            <Grid item xs>
                <FileInput
                    label="Image"
                    name="image"
                    onChange={fileChangeHandler}
                    error={getFieldError('image')}
                />
            </Grid>

            <Grid item xs={12}>
                <ButtonWithProgress
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    loading={loading}
                    disabled={loading}
                >
                    Create
                </ButtonWithProgress>
            </Grid>
        </Grid>
    );
};

export default AddPhotoForm;