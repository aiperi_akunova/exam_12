import React, {useEffect, useState} from 'react';
import {Link as RouterLink} from 'react-router-dom';
import {Avatar, Container, Grid, Link, makeStyles, Typography} from "@material-ui/core";
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import {useDispatch, useSelector} from "react-redux";
import {clearErrorUser, registerUser} from "../../store/actions/usersActions";
import FormElement from "../../components/UI/Form/FormElement";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";


const useStyles = makeStyles(theme =>({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        marginTop: theme.spacing(1),
    },
    submit:{
        margin: theme.spacing(3,0,2)
    },
}));



const Register = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const error = useSelector(state => state.users.registerError);
    const loading = useSelector(state => state.users.registerLoading);

    const [user, setUser] = useState({
        username: '',
        password: '',
        display_name: '',
    });

    useEffect(() => {
        return () => {
            dispatch(clearErrorUser())
        }
    }, [dispatch])


    const inputChangeHandler = e =>{

        const {name, value} = e.target;

        setUser(prevState => ({...prevState, [name]: value}))
    };

    const submitFormHandler = e=>{
        e.preventDefault();
       dispatch(registerUser({...user}))
    };


    const getFieldError = fieldName =>{
        try{
            return error.errors[fieldName].message;
        } catch (e){
            return undefined;
        }
    };



    return (
       <Container component="section" maxWidth="xs">
           <div className={classes.paper}>
               <Avatar className={classes.avatar}>
                   <LockOutlinedIcon/>
               </Avatar>
               <Typography component="h1" variant='h6'>
                   Sign up
               </Typography>
               <Grid
                   component="form"
                   container
                   className={classes.form}
                   onSubmit={submitFormHandler}
                   noValidate
               >

                   <FormElement
                       required
                       type="email"
                       label="Email"
                       onChange={inputChangeHandler}
                       name="username"
                       autoComplete="new-email"
                       value={user.email}
                       error={getFieldError('email')}
                       />

                   <FormElement
                       required
                       type="password"
                       label="Password"
                       onChange={inputChangeHandler}
                       name="password"
                       autoComplete="new-password"
                       value={user.password}
                       error={getFieldError('password')}
                   />

                   <FormElement
                       required
                       type="text"
                       label="Display name"
                       onChange={inputChangeHandler}
                       name="display_name"
                       value={user.display_name}
                       error={getFieldError('display_name')}
                   />

                   <Grid item xs={12}>
                       <ButtonWithProgress
                           type="submit"
                           fullWidth
                           variant="contained"
                           color="primary"
                           className={classes.submit}
                           loading={loading}
                           disabled={loading}
                       >
                           Sign up
                       </ButtonWithProgress>
                   </Grid>

                   <Grid item container justifyContent="flex-end">
                       <Link component={RouterLink} variant="body2" to="/login">
                           Already have an account? Sign in.
                       </Link>
                   </Grid>

               </Grid>
           </div>
       </Container>
    );
};

export default Register;