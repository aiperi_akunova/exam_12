import Layout from "./components/UI/Layout/Layout";
import {Redirect, Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {useSelector} from "react-redux";
import Main from "./containers/Main/Main";
import AddPhotoForm from "./containers/AddPhotoForm/AddPhotoForm";
import UsersPictures from "./containers/UsersPictures/UsersPictures";

const App = () => {
    const user = useSelector(state => state.users.user);

    const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };

    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={Main}/>
                {/*<Route path="/add" exact component={AddPhotoForm}/>*/}
                <ProtectedRoute path="/add" isAllowed={user} redirectTo="/login" component={AddPhotoForm}/>
                <Route path="/register" component={Register}/>
                <Route path="/users/:id" component={UsersPictures}/>
                <Route path="/login" component={Login}/>
            </Switch>
        </Layout>
    );
};

export default App;
