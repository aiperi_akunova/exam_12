import {Card, CardMedia, Grid, makeStyles,} from "@material-ui/core";
import {Link} from "react-router-dom";
import {apiURL} from "../../config";
import React, {useState} from "react";
import TransitionsModal from "../TransitionsModal/TransitionsModal";
import Fade from "@material-ui/core/Fade";
import theme from "../../theme";
import {useDispatch} from "react-redux";
import {deletePicture} from "../../store/actions/picturesActions";

const useStyles = makeStyles({
    card: {
        height: '100%',
        textAlign: "center"
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    },
    btn:{
        border: "2px solid darkgrey",
        padding: "5px 15px",
        backgroundColor: "white"
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        maxWidth: "80%",
    },
    img:{
        maxWidth: "700px",
        height: "100%",
    }
})

const PictureItem = ({title, user, picture, userId, showBtn, id}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const [open, setOpen] = useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    const img=apiURL+'/'+ picture;


    return (
        <Grid item xs={8} sm={6} md={4} lg={2}>
            <Card className={classes.card}>
                <CardMedia
                    image={img}
                    className={classes.media}
                    title={title}
                    onClick={handleOpen}
                />
                <h3>{title}</h3>
                <Link to={'/users/' + userId} className={classes.btn}>Added by {user}</Link>
                {showBtn && (
                    <button onClick={()=>dispatch(deletePicture(id))}>X</button>
                )}
            </Card>

            <TransitionsModal
                open={open}
                onClose={handleClose}
            >
                <Fade in={open}>
                         <div className={classes.paper}>
                             <button onClick={handleClose}>X</button>
                             <h2 id="transition-modal-title">{title}</h2>
                             <img src={img} alt='some pic' className={classes.img}/>

                         </div>
                     </Fade>
            </TransitionsModal>
        </Grid>
    );
};


export default PictureItem;