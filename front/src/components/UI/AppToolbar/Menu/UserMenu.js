import React from 'react';
import {Button, makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../../../store/actions/usersActions";


const useStyles = makeStyles(() =>({
    button: {
       borderLeft: "2px solid white",
        marginRight: "5px",
    },
    avatar: {
        width: "50px",
        height: "auto",
        borderRadius: "50%",
    },

}));

const UserMenu = ({user}) => {
    const classes= useStyles();
    const dispatch = useDispatch();
    return (
        <>
            <Button color="inherit" className={classes.button} component={Link} to={'/users/'+user._id}>
                {user.display_name}!
                {/*<img src={user.avatar } alt='avatar' className={classes.avatar}/>*/}
            </Button>
            <Button color="inherit" onClick={() => dispatch(logoutUser())} className={classes.button}>
                Log out
            </Button>
        </>
    );
};

export default UserMenu;