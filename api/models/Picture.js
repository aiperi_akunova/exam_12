const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const PictureSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },

})

PictureSchema.plugin(idValidator);
const Picture = mongoose.model('picture', PictureSchema);
module.exports = Picture;