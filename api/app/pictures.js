const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Picture = require('../models/Picture');
const auth = require("../middleware/auth");


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();


router.get('/',async (req, res) => {
  try {
    const pictures = await Picture.find().populate('user', 'display_name');
    res.send(pictures);

  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/:id',async (req, res) => {
  try {
    const pictures = await Picture.find({user: req.params.id}).populate('user', 'display_name');
    res.send(pictures);
  } catch (e) {
    res.sendStatus(500);
  }
});


router.post('/', auth, upload.single('image'), async (req, res) => {
  try {
    const picturesData = {
      user: req.user._id,
      title: req.body.title,
      image:'uploads/' + req.file.filename
    };

    const picture = new Picture(picturesData);
    await picture.save();
    res.send(picture);

  } catch(error) {
    console.log(error)
    res.status(400).send({error})
  }
});


router.delete('/:id', auth, async (req, res) => {
  try {
    const picture = await Picture.findByIdAndDelete(req.params.id);

    if (picture) {
      res.send(`Photo '${picture.title} removed'`);
    } else {
      res.status(404).send({error: 'Photo not found'});
    }

  } catch (e) {
    res.sendStatus(500);
  }
})


module.exports = router;