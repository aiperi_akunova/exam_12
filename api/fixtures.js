const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const User = require("./models/User");
const Picture = require("./models/Picture");


const run = async () => {
  await mongoose.connect(config.db.url);
  const collection = await mongoose.connection.db.listCollections().toArray();
  for (const coll of collection) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user1, user2, user3] = await User.create({
    username: 'john@gmail',
    password: 'doe',
    display_name: 'John Doe',
    token: nanoid(),
  }, {
    username: 'tom@gmail',
    password: 'jerry',
    display_name: 'Tom Jerry',
    token: nanoid(),
  }, {
    username: 'peri@gmail',
    password: '123',
    display_name: 'Perry',
    token: nanoid(),
  });

  await Picture.create({
      user: user1,
      title: 'Such a cute puppy!',
      image: 'fixtures/puppy.jpg',
    }, {
      user: user1,
      title: 'My lovely cat!',
      image: 'fixtures/kitten.jpeg',
    }, {
    user: user2,
    title: 'Best for weekends!',
    image: 'fixtures/house.jpg',
    },{
    user: user2,
    title: 'New Year mood',
    image: 'fixtures/room.jpg',
    },{
    user: user3,
    title: 'One of my biggest dream!',
    image: 'fixtures/castle.jpg',
    },{
    user: user3,
    title: 'Wanna be there now',
    image: 'fixtures/sun.jpg',
    }
  );

  await mongoose.connection.close();
};

run().catch(console.error);